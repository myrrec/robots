import com.myrrec.robots.Robot

import java.awt.Color

class RobotTest extends GroovyTestCase {

    def robot = new Robot(0,0,0,0, Color.black);
    def robot2 = new Robot(0,1,3,3,Color.BLACK);

    void testDefaults(){

        assertNull(robot.destination_robot);
        assertFalse(robot.moving);
        assertTrue(robot.alive);

    }

    void testGetDistance(){
        assertEquals(robot.getDistance(robot2),4);
    }

    void testCollidedWith(){
        assertTrue(robot.collidedWith(robot2));

    }


}
