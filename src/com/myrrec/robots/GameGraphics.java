package com.myrrec.robots;

import java.awt.*;

public class GameGraphics {

    /**
     * Установка сглаживания
     */
    public static Graphics2D antialias(Graphics2D g) {
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        return g;
    }

}
