package com.myrrec.robots;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Учитель для нейронной сети
 */

public class NeuralNetworkTeacher {

    public static final int input_neurons = Const.INPUT_NEURONS;

    public static void main(final String args[]) throws FileNotFoundException {

        double[][] examples_input = NeuralNetworkExample.input;
        double[][] examples_wanted_answer = NeuralNetworkExample.wanted;

        /**
         * Нейронная сеть
         */
        BasicNetwork network = NeuralNetwork.load();
        /**
         * Входной слой с n элементами
         */
        network.addLayer(new BasicLayer(null, true, input_neurons));

        /**
         * Скрытые нейроны
         */
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true,
                calcHiddenNeuronsCount(examples_input[0].length)));

        /**
         * Вывод -- одно число
         */
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 1));

        /**
         * Фиксируем структуру сети
         */
        network.getStructure().finalizeStructure();

        /**
         * Обнуляем веса
         */
        network.reset();

        /**
         * Набор данных для обучения
         */
        MLDataSet trainingSet = new BasicMLDataSet(examples_input, examples_wanted_answer);

        /**
         * Обучаем нейронную сеть
         */
        final ResilientPropagation train = new ResilientPropagation(network, trainingSet);

        /**
         * Итерируем до тех пор, пока погрешность не будет меньше одной сотой
         */
        int epoch = 1;

        do {
            train.iteration();
            if(Const.DEBUG) {
                System.out.println("Итерация #" + epoch + " Ошибка:" + train.getError());
            }
            epoch++;
        } while (train.getError() > 0.01);


        train.finishTraining();

        /**
         * Тестирование
         */

        if(Const.DEBUG) {

            System.out.println("Результаты:");

            for (MLDataPair pair : trainingSet) {
                final MLData output = network.compute(pair.getInput());
                System.out.println(pair.getInput().getData(0) + "," + pair.getInput().getData(1)
                        + ", фактический=" + output.getData(0) + ",желаемый=" + pair.getIdeal().getData(0));
            }

        }

        /**
         * Сохраняем сеть в файл
         */
        EncogDirectoryPersistence.saveObject(Const.TRAINED_ANN_PATH, network);

        /**
         * Убиваем учителя
         */
        Encog.getInstance().shutdown();
    }

    /**
     * Высчитывается по следствию из теорем Арнольда – Колмогорова – Хехт-Нильсена
     * @param examples_quantity Кол-во примеров
     * @return Количество скрытых нейронов в слое
     */
    private static int calcHiddenNeuronsCount(int examples_quantity) {

        int inputDimension = input_neurons;
        int outputDimension = 1;

        /**
         * Минимальные и максимальные значения
         */
        double min = (outputDimension * examples_quantity) / (1 + (Math.log(examples_quantity) / Math.log(2)));
        double max = outputDimension * (outputDimension + 1 + inputDimension) * (examples_quantity / inputDimension + 1)
                + outputDimension;

        /**
         * Кол-во скрытых нейронов
         */
        double hidden_neurons = min + (Math.random() * ((max - min) + 1));


        return (int) Math.ceil(hidden_neurons);

    }
}