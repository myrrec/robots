package com.myrrec.robots;

import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.persist.EncogDirectoryPersistence;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author Михаил
 */
public class NeuralNetwork {

    private static Robots robots = Robots.instance();

    public static BasicNetwork load() throws FileNotFoundException {

        /**
         * Если уже есть используемая нейронная сеть, возвращаем её
         */
        if (robots.network != null) {
            return robots.network;
        } else {

            File network_file = Const.TRAINED_ANN_PATH;

            /**
             * Если такой файл найден в директории, пишем его в глобальный массив и возвращаем
             */
            if (network_file.exists() && !network_file.isDirectory()) {

                BasicNetwork network = (BasicNetwork) EncogDirectoryPersistence.loadObject(network_file);

                robots.network = network;

                return network;

            } else {

                /**
                 * В противном случае предупреждаем
                 */
                throw new FileNotFoundException("Не найден файл нейросети");
            }

        }

    }

    /**
     * Главный метод нейросети. Возвращает направление робота в зависимости от обстановки
     *
     * @return Направление движения
     */
    public int calculateMovement(Robot me, Robot enemy) throws Exception {

        if (robots.network == null) {
            load();
        }

        if (me == null || enemy == null) {
            throw new Exception("Ни один из роботов не может равняться нулю");
        }

        /**
         * Информация для нейросети
         */
        double[] input_info = new double[]{
                me.health,
                enemy.health,
                me.getDistance(enemy),
                enemy.countNearest(false),
                enemy.countNearest(true)
        };

        /**
         * Превращаем её в читаемый формат
         */
        MLData input_data = new BasicMLData(input_info);

        /**
         * Компилируем и преобразовываем в дабл
         */
        double output = robots.network.compute(input_data).getData(0);

        return (int) Math.ceil(output);

    }

}
