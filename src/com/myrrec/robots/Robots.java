package com.myrrec.robots;

import org.encog.neural.networks.BasicNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class Robots {

    /**
     * Глобальный список активных роботов
     */
    public CopyOnWriteArrayList<Robot> robots = new CopyOnWriteArrayList<>();

    /**
     * Глобальный ассоциативный массив движений роботов.  Ключ -- id движущегося робота
     */
    public HashMap<Integer, RobotMovement> movements = new HashMap<>();

    /**
     * Фрейм
     */
    public Frame frame;

    /**
     * Панель, на которой располагаются роботы
     */
    public RobotsPanel robotsPanel;

    /**
     * Панель настроек и информации
     */
    public SettingsPanel settingsPanel;

    /**
     * Победившая команда (по умолчанию -1)
     */
    public int won_team = -1;

    /**
     * Нейронная сеть, используемая для принятия решений
     */
    public BasicNetwork network = null;

    /**
     * Реализация синглтона
     */
    private Robots() { }
    private static Robots instance = new Robots();
    public static Robots instance() {
        return instance;
    }

    public void add(Robot robot) {
        robots.add(robot);
    }

    /**
     * Получает всех роботов
     */
    public Robot[] getAll(boolean aliveOnly) {

        if (aliveOnly) {

            ArrayList<Robot> out = new ArrayList<>();

            if (robots.size() > 0) {
                try {
                    for (Robot r : robots) {
                        if (r.alive) {
                            out.add(r);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return out.toArray(new Robot[out.size()]);
        } else {
            return robots.toArray(new Robot[robots.size()]);
        }
    }

    /**
     * Получает робота по id
     */
    public Robot getById(int id) {
        if (id < robots.size()) {
            return robots.get(id);
        } else {
            throw new ArrayIndexOutOfBoundsException("Id робота не может быть больше размера массива");
        }
    }

    /**
     * Получает всех живых роботов по командам
     */
    public HashMap<Integer, ArrayList<Robot>> getByTeams() {

        HashMap<Integer, ArrayList<Robot>> out = new HashMap<>();

        for (Robot r : robots) {

            if (r.alive) {

                ArrayList<Robot> current_in_team = out.get(r.team_number);

                if (current_in_team == null) {
                    current_in_team = new ArrayList<>();
                }

                current_in_team.add(r);

                out.put(r.team_number, current_in_team);

            }
        }

        return out;
    }
    /**
     * Список живых роботов
     */
    public Robot[] getByFriends(Robot robot, boolean friends){

        int robot_team = robot.team_number;

        ArrayList<Robot> output = new ArrayList<>();

        Robot[] all = getAll(true);

        for (Robot r : all) {
            if(friends){
                if (r.team_number == robot_team) {
                    output.add(r);
                }
            }else{
                if (r.team_number != robot_team) {
                    output.add(r);
                }
            }
        }

        return output.toArray(new Robot[output.size()]);

    }

    /**
     * Убирает всё связанное с роботами
     */
    public void clear(){

        /**
         * Убираем роботов
         */
        settingsPanel.clearRobotsCount();
        robotsPanel.removeAll();

        /**
         * Перерисовка окна для отображения
         */
        frame.repaint();

        settingsPanel.result.setText("Результат:");

    }

}
