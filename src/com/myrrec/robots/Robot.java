package com.myrrec.robots;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Класс робота
 * Боевая единица
 */

public class Robot extends JPanel {

    public final int id, team_number;

    public Color color;

    public int health = Const.ROBOT_HEALTH;

    public boolean moving = false, alive = true;

    public Robot destination_robot = null;

    public Robot(int robot_id, int robot_team_number, int x, int y, Color robot_color) {

        id = robot_id;
        team_number = robot_team_number;
        color = robot_color;

        setLocation(x, y);
        setName("Робот " + id);
        setSize(Const.ROBOT_DIAMETER, Const.ROBOT_DIAMETER);

        /**
         * Отслеживаем удары данного робота
         */
        new RobotHitsHandler(this);

    }

    /**
     * Расстояние между текущим роботом и заданным
     */
    public int getDistance(Robot robot) {

        int x_dist = Math.abs(this.getX() - robot.getX());
        int y_dist = Math.abs(this.getY() - robot.getY());

        return (int) Math.round(Math.sqrt(Math.pow(x_dist, 2) + Math.pow(y_dist, 2)));

    }

    /**
     * Все роботы, с которыми в данный момент столкнулся текущий
     */
    public Robot[] getCollidedRobots() {

        ArrayList<Robot> output = new ArrayList<>();
        Robot[] enemies = Robots.instance().getByFriends(this,false);

        for (Robot enemy : enemies) {
            if (collidedWith(enemy)) {
                output.add(enemy);
            }
        }

        return output.toArray(new Robot[output.size()]);
    }

    /**
     *  Количество роботов, находящихся рядом с данным
     */
    public int countNearest(boolean enemies){
        int count = 0;
        Robots robots = Robots.instance();

        Robot[] others = robots.getByFriends(this, enemies);

        for (Robot some_robot : others) {
            if (some_robot.getDistance(this) <= Const.NEAR_DISTANCE) {
                count++;
            }
        }

        return count;
    }

    /**
     * Достаточно ли расстояние между текущим и заданным роботом для удара
     */
    public boolean collidedWith(Robot robot){
        return getDistance(robot) <= Const.ROBOT_DIAMETER;
    }

    /**
     * Отрисовывает робота на панели
     */
    public void paintComponent(Graphics graphics) {

        Graphics2D g = (Graphics2D) graphics;

        if (!Const.DEBUG) {
            g = GameGraphics.antialias(g);
        }

        g.setColor(color);
        g.fillOval(0, 0, Const.ROBOT_DIAMETER, Const.ROBOT_DIAMETER);

    }

}
