package com.myrrec.robots;

import java.awt.*;
import java.io.File;

public final class Const {

    public static final boolean DEBUG = true;
    public static final boolean CONSOLE_ONLY = false;

    public static final int WINDOW_WIDTH = 1200;
    public static final int WINDOW_HEIGHT = 700;

    public static final double PANELS_PROPORTION = 4.5 / 6.0;

    public static final Color ROBOTS_PANEL_BACKGROUND = Color.black;
    public static final Color SETTINGS_PANEL_BACKGROUND = Color.white;
    public static final Color SPAWN_AREA_COLOR = Color.white;
    public static final Color BANGED_ROBOT_COLOR = Color.red;

    public static final String ROBOTS_PANEL_NAME = "Панель роботов";
    public static final String SETTINGS_PANEL_NAME = "Панель настроек";
    public static final String WINDOW_HEADER = "Роботы";

    public static final int ROBOT_HEALTH = 100;
    public static final int ROBOT_DIAMETER = 10;
    public static final int CHECK_IF_WON_DELAY = 2000;

    /**
     * Расстояние, находясь на котором, роботы считаются ближайшими
     */
    public static final int NEAR_DISTANCE = 2 * ROBOT_DIAMETER;

    /**
     * Задержка после перемещения на пиксель, миллисекунды
     */
    public static final int SLEEP_AFTER_STEP = 25;

    /**
     * Насколько быстро робот меняет свой цвет после удара
     */
    public static final int BREAK_BETWEEN_BANG = 1000;

    public static final Font TEAMS_INFO_FONT = new Font("Arial",Font.BOLD,30);

    /**
     * Количество входных нейронов для нейросети
     * Параметры:
     *  Здоровье робота(0.0 -- 100.0)
     *  Здоровье врага(0.0 -- 100.0)
     *  Расстояние до врага (0.0 -- MAX)
     *  Направление движения врага(0 -- к тебе, 1 -- от тебя, 2 -- в прочие стороны)
     *  Сколько друзей рядом с врагом (0 -- MAX_GOOD)
     *  Сколько врагов рядом с врагом (0 -- MAX_BAD)
     */
    public static final int INPUT_NEURONS = 6;

    /**
     * Где искать файл обученной нейронной сети
     */

    public static final File TRAINED_ANN_PATH = new File("data/ANN/network.eg");

}
