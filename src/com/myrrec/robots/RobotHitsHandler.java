package com.myrrec.robots;

/**
 * Обработчик удара робота
 */
public class RobotHitsHandler extends Thread {

    private final Robot robot;

    public RobotHitsHandler(Robot input_robot) {

        robot = input_robot;

        start();

    }

    public void run() {


        boolean alive = robot.alive;

        while (alive) {

            Robot[] enemies = robot.getCollidedRobots();

            if (enemies.length > 0) {

                for (Robot enemy : enemies) {

                    new RobotHit(enemy);

                    try {
                        sleep(Const.BREAK_BETWEEN_BANG);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            alive = Robots.instance().getById(robot.id).alive;
        }

    }

}
