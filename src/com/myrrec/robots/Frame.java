package com.myrrec.robots;

/**
 * Окно приложения
 */

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Frame extends JFrame {

    public Frame() {

        /** Заголовок окна */
        super(Const.WINDOW_HEADER);

        /** Размер окна */
        setSize(Const.WINDOW_WIDTH, Const.WINDOW_HEIGHT);

        setResizable(false);

        /**
         * Установка иконок
         */
        ArrayList<Image> icons = new ArrayList<>();

        icons.add(new ImageIcon("data/icons/icon_16.png").getImage());
        icons.add(new ImageIcon("data/icons/icon_32.png").getImage());
        icons.add(new ImageIcon("data/icons/icon_64.png").getImage());
        icons.add(new ImageIcon("data/icons/icon_128.png").getImage());
        icons.add(new ImageIcon("data/icons/icon_256.png").getImage());
        icons.add(new ImageIcon("data/icons/icon_512.png").getImage());

        setIconImages(icons);

        /** Центрирование окна */
        setLocationRelativeTo(null);

        /** Обработка закрытия программы */
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        /** Видимость окна */
        setVisible(!Const.CONSOLE_ONLY);

        setDefaultLookAndFeelDecorated(true);

    }
}