package com.myrrec.robots;

/**
 * Левый верхний угол, от которого строятся роботы
 * Выводится только в случае com.myrrec.robots.Const.Debug == true
 */

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class SpawnArea extends JPanel {

    private final static Robots robots = Robots.instance();
    private final static RobotsPanel panel = robots.robotsPanel;
    private static ArrayList<int[]> spawn_points = new ArrayList<>();
    int length;

    public SpawnArea(int x, int y, int area_length) {

        length = area_length;
        setLocation(x, y);

        /**
         * Размер подложки, на которой будет рисоваться точка,
         * т.е. квадратный корень из общего числа роботов
         * умножить на диаметр каждого робота плюс 1 пиксель
         * для отображения границ
         */
        setSize(length * Const.ROBOT_DIAMETER + 1, length * Const.ROBOT_DIAMETER + 1);

    }

    /**
     * Метод, проверяющий расстояние между заданной точкой и остальными на диапазон длины
     */
    private static boolean checkSpawnPoint(int x, int y, double distanceMin, double distanceMax) {
        for (int[] point : spawn_points) {

            int diffX = Math.abs(x - point[0]);
            int diffY = Math.abs(y - point[1]);

            /**
             * Если значения выходят за рамки диапазона
             */
            if (diffX < distanceMin || diffX > distanceMax
                    || diffY < distanceMin || diffY > distanceMax) {
                return false;
            }
        }

        return true;
    }

    /**
     * Метод, генерирующий точку спауна в заданном диапазоне
     */
    private static int[] makeSpawnPoint(double distanceMin, double distanceMax, int team) {

        RobotsPanel panel = new RobotsPanel();

        /**
         * Случайные значения от distanceMin до distanceMax
         */
        int[] output = new int[]{
                (int) distanceMin + (int) (Math.random() * ((distanceMax - distanceMin) + 1)),
                (int) distanceMin + (int) (Math.random() * ((distanceMax - distanceMin) + 1))
        };

        /**
         * Если при заданной точке команда роботов не будет помещаться на поле, переделываем
         */
        if (output[0] > panel.getWidth() - Math.ceil(Math.sqrt(team)) ||
                output[1] > panel.getHeight() - Math.ceil(Math.sqrt(team))) {
            output = makeSpawnPoint(distanceMin, distanceMax, team);
        }

        return output;
    }

    /**
     * Вычисление точек спауна для массива команд
     */
    private static int[][] calcSpawnPoints(int[] teams) {

        int[][] output = new int[spawn_points.size()][2];

        for (int team : teams) {

            double distanceMin = Const.ROBOT_DIAMETER + Math.ceil(Math.sqrt(team)) * 2;
            double distanceMax = Math.min(Const.WINDOW_HEIGHT, Const.WINDOW_WIDTH) - distanceMin;

            int[] spawn_point = makeSpawnPoint(distanceMin, distanceMax, team);

            while (!checkSpawnPoint(spawn_point[0], spawn_point[1], distanceMin, distanceMax)) {
                spawn_point = makeSpawnPoint(distanceMin, distanceMax, team);
            }

            spawn_points.add(spawn_point);

        }

        return spawn_points.toArray(output);

    }

    /**
     * Генерация count роботов около точки спауна
     */
    private static void spawnAround(
            int x, int y, int count, int team_number, int id_start, Color color) {

        int rows = (int) Math.ceil(Math.sqrt(count));

        if (Const.DEBUG) {
            SpawnArea area = new SpawnArea(x, y, rows);
            panel.add(area);
        }

        int iterated = 0;

        for (int i = 0; i < rows; i++) {

            /**
             * Если ещё много роботов, то сгенерим по rows в ряд, если ряд последний, то генерим остаток
             */
            int robots_in_a_row = Math.min(rows, count - rows * i);

            for (int m = 0; m < robots_in_a_row; m++) {

                int X = x + m * Const.ROBOT_DIAMETER;
                int Y = y + i * Const.ROBOT_DIAMETER;

                /**
                 * Создание робота на панели
                 */
                com.myrrec.robots.Robot robot =
                        new com.myrrec.robots.Robot(iterated + id_start, team_number, X, Y, color);

                /**
                 * Занесение в массив роботов
                 */
                robots.add(robot);

                panel.add((robot));

                iterated++;
            }
        }

        if (Const.DEBUG) {
            App.printTeamInfo(team_number, x, y, count, color);
        }
    }

    /**
     * Сумма всех элементов массива до index элемента
     */
    private static int sumBeforeIndex(int[] array, int index) {

        int result = 0;

        for (int i = 0; i < index; i++) {
            result += array[i];
        }

        return result;
    }

    /**
     * @param teams  Кол-во роботов в командах
     * @param colors Цвета роботов
     */
    public static void spawnTeams(int[] teams, Color[] colors) {

        spawn_points.clear();

        int[][] spawn_points = calcSpawnPoints(teams);

        for (int i = 0; i < spawn_points.length; i++) {

            Color color = colors[i];

            spawnAround(spawn_points[i][0], spawn_points[i][1], teams[i], i + 1, sumBeforeIndex(teams, i), color);
        }

    }

    /**
     * Отрисовка на панели
     */
    public void paintComponent(Graphics graphics) {

        Graphics2D g = (Graphics2D) graphics;
        g = GameGraphics.antialias(g);

        g.setColor(Const.SPAWN_AREA_COLOR);
        g.drawRect(0, 0, length * Const.ROBOT_DIAMETER, length * Const.ROBOT_DIAMETER);
    }

}