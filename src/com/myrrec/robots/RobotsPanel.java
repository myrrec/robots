package com.myrrec.robots;

import javax.swing.*;

public class RobotsPanel extends JPanel {

    public RobotsPanel() {

        setBackground(Const.ROBOTS_PANEL_BACKGROUND);

        setLayout(null);

        setName(Const.ROBOTS_PANEL_NAME);

        int width = (int) (Const.WINDOW_WIDTH * Const.PANELS_PROPORTION);

        setBounds(0, 0, width, Const.WINDOW_HEIGHT);
    }

}