package com.myrrec.robots;

import javax.swing.*;
import java.awt.*;

public class App {

    public static void make() {

        /**--------------*/
        Color[] colors = new Color[]{
                new Color(84, 172, 114), new Color(70, 103, 195),
                new Color(207, 78, 47), new Color(66, 207, 91),
                new Color(156, 129, 185), new Color(96, 65, 195),
        };
        /**--------------*/

        /**
         * Создание окна
         */
        Frame frame = new Frame();

        /**
         * Создание панелей и добавление их в окно, вывод отладочной инфорамции при необходимости
         */
        RobotsPanel robotsPanel = new RobotsPanel();
        SettingsPanel settingsPanel = new SettingsPanel();

        frame.add(settingsPanel);
        frame.add(robotsPanel);

        if (Const.DEBUG) {
            printFrameInfo(frame);
        }

        /**
         * Занесение в глобальный объект панели, на которой роботы располагаются
         */
        Robots robots = Robots.instance();
        robots.settingsPanel = settingsPanel;
        robots.robotsPanel = robotsPanel;
        robots.frame = frame;

        /**
         * Обработчик нажатия клавиш. В нём также обрабатывается генерация роботов
         */
        new EventsHandler(frame, settingsPanel, colors);

        /**
         * Перерисовка окна для отображения
         */
        frame.repaint();

        //new RobotMovement(robots.getById(0),robots.getById(1));

        /**-------------------*
         for (int i = 0; i < Robots.instance().robots.size(); i++) {

         int to = i + (int) (Math.random() * ((Robots.instance().robots.size() - 1 - i) + 1));

         new RobotMovement(robots.getById(i), robots.getById(to));
         }
         /**-------------------*/

    }

    public static void printFrameInfo(JFrame frame) {

        System.out.println("Окно: " + frame.getBounds());

        for (Component component : frame.getContentPane().getComponents()) {

            JPanel panel = (JPanel) component;

            System.out.println(panel.getName() + ": " + panel.getBounds());


        }
    }

    public static void printTeamInfo(int team_number, int x, int y, int count, Color color) {
        System.out.println("Команда №" + team_number + " \n\t Точка спауна: " + x + ":" + y + "\n\t Кол-во роботов: " +
                count + "\n\t Цвет: " + color);
    }

}
