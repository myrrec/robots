package com.myrrec.robots;

/**
 * Обработчик удара по роботу
 */

import java.awt.*;

public class RobotHit extends Thread {

    private final Robot robot;

    private final int hit = getBang();

    private Robots robots = Robots.instance();

    public RobotHit(Robot robot_input) {

        robot = robot_input;

        setName("Удар по " + robot.id);

        start();

    }

    /**
     * Робот наносит удар от 10 до 20
     */
    private static int getBang() {
        return (int) (Math.random() * 10) + 10;
    }

    public void run() {

        /**
         * Сохранение оригинального цвета (для восстановления после изменения)
         */
        Color original_color = robot.color;

        robot.color = Const.BANGED_ROBOT_COLOR;

        robots.frame.repaint();

        /**
         * Задержка для анимации
         */
        try {
            sleep(Const.BREAK_BETWEEN_BANG / 2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (robot.health > hit) {

            robot.color = original_color;

        } else {

            /**
             * На панели роботов делаем панель прозрачной
             */
            robot.color = new Color(0, 0, 0, 0);

            /**
             * В глобальном массиве ставим, что этот робот мёртв
             */
            robots.getById(robot.id).alive = false;

            if (Const.DEBUG) {
                System.out.println(robot.id + " умер");
            }

            /**
             * На панели настроек минусуем общее количество роботов
             */
            robots.settingsPanel.updateRobotsCount(robot.team_number);

        }

        /**
         * Здоровье не может быть ниже нуля
         */
        if (hit >= robot.health || robot.health < 0) {
            robot.health = 0;
        } else {
            robot.health -= hit;
        }

        robots.frame.repaint();

    }

}
