package com.myrrec.robots;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Михаил
 */
public class VictoryCheck extends Thread {

    public VictoryCheck() {
        start();
    }

    public void run() {

        HashMap<Integer, ArrayList<Robot>> robots_teams = Robots.instance().getByTeams();

        int size = robots_teams.size();

        while (size > 1) {
            /**
             * Если осталась одна команда или 0, продолжать смысла нет
             */
            if (size < 2) {
                break;
            }

            robots_teams = Robots.instance().getByTeams();

            size = robots_teams.size();


            try {
                sleep(Const.CHECK_IF_WON_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        /**
         * Если осталась одна команда
         */
        if (size == 1) {

            /**
             * Получаем множество ключей из одного элемента, преобразовываем его в массив и достаём единственный
             * элемент, его и присваиваем
             */
            Set<Integer> keys_set = robots_teams.keySet();
            Integer[] keys_array = keys_set.toArray(new Integer[keys_set.size()]);

            Robots.instance().won_team = keys_array[0];

            /**
             * Выводим на экран и в консоль
             */
            print(keys_array[0]);

        } else {
            /**
             * Все проиграли
             */
            print(-1);
        }

    }

    public void print(int won_team) {

        String message;

        message = won_team == -1 ? "ничья" : "победила команда " + won_team;

        if (Const.DEBUG) {
            System.out.println(message);
        }

        Robots.instance().robotsPanel.removeAll();


        SettingsPanel.result.setText("Результат: " + message);

    }
}
