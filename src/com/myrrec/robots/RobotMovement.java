package com.myrrec.robots;

/**
 * Класс, который обеспечивает асинхронное движение роботов из-за потоков
 * Один поток -- движения для робота
 */

public class RobotMovement extends Thread {

    /**
     * Робот, к которому движется текущий
     */
    private final Robot destination_robot;

    /**
     * Движущийся робот
     */
    private final Robot robot;

    private Robots robots = Robots.instance();

    public RobotMovement(Robot moving_robot, Robot dest_robot) {

        robot = moving_robot;
        destination_robot = dest_robot;

        robots.movements.put(robot.id, this);

        setName(moving_robot.getName() + " -> " + dest_robot.getName());

        start();

    }

    private static int isPositive(int x) {
        return (x > 0) ? 1 : (x < 0) ? -1 : 0;
    }

    public void removeAndStop() {

        robots.movements.remove(robot.id);
        robots.getById(robot.id).moving = false;
        robots.getById(robot.id).destination_robot = null;

    }

    public void run() {

        int x, y, projection_x, projection_y, shift_x, shift_y, pdx, pdy, es, el, err;

        /**
         * Запись данных о перемещении робота в глобальный массив (для того, чтобы его могли отслеживать другие)
         */

        Robot robot_global = robots.getById(robot.id);
        robot_global.moving = true;
        robot_global.destination_robot = destination_robot;


        int destination_x = destination_robot.getX();
        int destination_y = destination_robot.getY();

        /**
         * Обработка при указании неверных координат
         */

        RobotsPanel panel = new RobotsPanel();

        int minX = panel.getX(), minY = panel.getY();
        int maxX = panel.getWidth() - Const.ROBOT_DIAMETER;
        int maxY = panel.getHeight() - Const.ROBOT_DIAMETER;

        if (destination_x > maxX) {
            destination_x = maxX;
        } else if (destination_x < minX) {
            destination_x = minX;
        }

        if (destination_y > maxY) destination_y = maxY;
        else if (destination_y < minY) destination_y = minY;


        if (Const.DEBUG) {
            System.out.println(this.getName());
        }

        int original_x = robot.getX();
        int original_y = robot.getY();

        /**
         * Проекции на оси
         */
        projection_x = destination_x - original_x;
        projection_y = destination_y - original_y;

        /**
         * Определение, в какую сторону сдвигается прямая
         */
        shift_x = isPositive(projection_x);
        shift_y = isPositive(projection_y);

        /**
         * Вычисление модулей проекций
         */

        projection_x = Math.abs(projection_x);
        projection_y = Math.abs(projection_y);

        /**
         * Определение наклона отрезка
         */
        if (projection_x > projection_y) {
            /**
             * Если projection_x > projection_y, то значит отрезок "вытянут" вдоль оси икс, т.е. он скорее длинный, чем
             * высокий. Значит в цикле нужно будет идти по икс (строчка el = projection_x;), значит "протягивать" прямую
             * по иксу надо в соответствии с тем, слева направо и справа налево она идёт (pdx = shift_x;), при этом по y
             * сдвиг такой отсутствует.
             */
            pdx = shift_x;
            pdy = 0;
            es = projection_y;
            el = projection_x;

        } else {
            /**
             * Случай, когда прямая скорее "высокая", чем длинная, т.е. вытянута по оси y, тогда в цикле двигаемся по у
             */
            pdx = 0;
            pdy = shift_y;
            es = projection_x;
            el = projection_y;
        }

        /**
         * Все последующие точки возможно надо сдвигать, поэтому первую ставим вне цикла
         */
        x = original_x;
        y = original_y;
        err = el / 2;
        robot.setLocation(x, y);

        /**
         * Проход по всем точкам от второй до последней
         */
        for (int t = 0; t < el; t++) {
            err -= es;

            /**
             * Если робот после перерисовки достиг робота (на полпути, например), либо
             * поток, отвечающий за его движение, был остановлен, либо
             * цель уже повержена, останавливаем процесс
             */

            if (!robots.movements.containsKey(robot.id) || robot.collidedWith(destination_robot) ||
                    !destination_robot.alive) {
                return;
            }


            if (err < 0) {
                /**
                 * Сместить вверх или вниз, если цикл проходит по X или сместить влево-вправо, если цикл проходит по Y
                 */
                err += el;
                x += shift_x;
                y += shift_y;
            } else {
                /**
                 * Сдвинуть влево или вправо, если цикл идёт по иксу; сдвинуть вверх или вниз, если по y
                 */
                x += pdx;
                y += pdy;
            }

            /**
             * Перерисовываем робота
             */
            robot.setLocation(x, y);

            /**
             * Для эффекта движения задерживаемся
             */
            try {
                Thread.sleep(Const.SLEEP_AFTER_STEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /**
         * Обновляем значение в глобальном массиве
         */
        robot_global.moving = false;

        if (Const.DEBUG) {
            System.out.println(robot.id + " закончил движение");
        }

        removeAndStop();

    }

    public void interrupt() {

        if (Const.DEBUG) {
            System.out.println("Поток '" + getName() + "' остановлен");
        }
        removeAndStop();

    }
}