package com.myrrec.robots;

import javax.swing.*;
import java.awt.*;


public class SettingsPanel extends JPanel {


    public static JLabel result;
    public static JLabel[] teams;
    /**
     * Текстовое поле, куда вводятся команды роботов
     */
    public JTextField teams_input;
    /**
     * Кнопка, по нажатию которой начинается сражение
     */
    public JButton start_button;
    /**
     * Кнопка, которая очищает поле
     */
    public JButton clear_button;


    public SettingsPanel() {

        setBackground(Const.SETTINGS_PANEL_BACKGROUND);
        setName(Const.SETTINGS_PANEL_NAME);

        setLayout(null);

        int width = (int) (Const.WINDOW_WIDTH * (1 - Const.PANELS_PROPORTION));
        setBounds((int) (Const.WINDOW_WIDTH * Const.PANELS_PROPORTION), 0, width, Const.WINDOW_HEIGHT);

        setUpUI();
    }

    public void setUpUI() {

        JLabel teams_label = new JLabel("Введите кол-во роботов в командах:");
        teams_label.setBounds(30, 0, 250, 30);
        add(teams_label);

        teams_input = new JTextField();
        teams_input.grabFocus();
        teams_input.setBounds(30, 30, 240, 30);
        add(teams_input);

        clear_button = new JButton("Очистить поле");
        clear_button.setBounds(30, 70, 120, 30);
        add(clear_button);

        start_button = new JButton("Запуск");
        start_button.setBounds(150, 70, 120, 30);
        add(start_button);

        result = new JLabel("Результат:");
        result.setBounds(30, 110, 240, 50);
        add(result);
    }

    /**
     * Рисует цифры -- количество роботов на панели настроек
     */
    public void paintRobotsCount(Color[] colors, int[] count) {

        if (colors.length < count.length) {
            throw new IllegalArgumentException("Кол-во цветов не может быть меньше кол-ва роботов");
        }

        /**
         * Смещение после отрисовки очередной цифры
         */
        int shift_y = Const.TEAMS_INFO_FONT.getSize();

        teams = new JLabel[colors.length];

        for (int i = 0; i < count.length; i++) {

            JLabel team = new JLabel();

            try {
                team.setText(count[i] + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            team.setForeground(colors[i]);
            team.setBounds(30, 160 + shift_y * i, 50, 50);
            team.setFont(Const.TEAMS_INFO_FONT);

            add(team);

            teams[i] = team;
        }

        repaint();

    }

    public void updateRobotsCount(int team_id) {

        String original_value = teams[team_id - 1].getText();

        teams[team_id - 1].setText((Integer.parseInt(original_value) - 1) + "");

        repaint();

    }

    public void clearRobotsCount() {

        if (teams == null || teams.length == 0) {
            return;
        }

        for (JLabel team : teams) {
            if(team != null) {
                remove(team);
            }
        }
    }
}