package com.myrrec.robots;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Михаил
 */
public class EventsHandler {

    public EventsHandler(final Frame frame, final SettingsPanel settingsPanel, final Color[] colors) {


        frame.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent e) {

                /**
                 * При нажатии клавиши esc выходим из приложения
                 */
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    System.exit(0);
                }
            }

        });

        /**
         * Обработчик событий при нажатии на кнопку запуска
         */
        settingsPanel.start_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int[] count_int;

                /**
                 * Получаем значение из teams_input
                 */
                String teams_string = settingsPanel.teams_input.getText();

                /**
                 * Пустую строку обрабатывать не нужно
                 */
                if (teams_string.equals("")) {
                    return;
                }

                /**
                 * Разбиваем по пробелу, создаём числовой массив того же размера
                 */
                String[] count_string = teams_string.split("\\s");

                count_int = new int[count_string.length];

                /**
                 * Перегоняем в числовой массив
                 */
                for (int i = 0; i < count_string.length; i++) {
                    count_int[i] = Integer.parseInt(count_string[i]);
                }

                if(Const.DEBUG){
                    System.out.println("Игра началась");
                }

                /**
                 * Удаление прошлых роботов
                 */
                Robots.instance().clear();

                /**
                 * Отрисовка количества роботов на панели настроек
                 */
                settingsPanel.paintRobotsCount(colors, count_int);

                /**
                 * Отрисовка команд, указанных в robots_teams
                 */
                SpawnArea.spawnTeams(count_int, colors);

                /**
                 * Перерисовка окна для отображения
                 */
                frame.repaint();

                /**
                 * Обработчик победы
                 */
                new VictoryCheck();

            }
        });

        /**
         * Обработчик при нажатии на кнопку "Очистить поле"
         */
        settingsPanel.clear_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Robots.instance().clear();
            }
        });

    }

}
